'use strict';

var format = function(str, data) {
  return str.replace(/{([^{}]+)}/g, function(match, val) {
    var prop = data;
    val.split('.').forEach(function(key) {
      prop = prop[key];
    });

    return prop;
  });
};

String.prototype.format = function(data) {
  return format(this, data);
};

String.prototype.encodedURI = function() {
  return this.replace(' ', '+');
};

String.prototype.slugify = function() {
  function dasherize(str) {
    return str.trim().replace(/[-_\s]+/g, '-').toLowerCase();
  }

  function clearSpecial(str) {
    var from  = 'ąàáäâãåæăćčĉęèéëêĝĥìíïîĵłľńňòóöőôõðøśșşšŝťțţŭùúüűûñÿýçżźž',
      to    = 'aaaaaaaaaccceeeeeghiiiijllnnoooooooossssstttuuuuuunyyczzz';
    to = to.split('');
    return str.replace(/.{1}/g, function(c){
      var index = from.indexOf(c);
      return index === -1 ? c : to[index];
    });
  }

  return clearSpecial(dasherize(this));
};

Number.prototype.paddingLeft = function(size, char) {
  if (!char) {
    char = '0'
  }
  var length = this.toString().length;
  if (length >= size) {
    return this.toString();
  }
  var result = [];
  for (var i = length; i < size; i++) {
    result.push(char);
  }
  return result.join('') + this.toString();
};

Number.prototype.formataDecimal = function(isCurrency) {
  if (isCurrency === undefined) {
    isCurrency = false;
  }
  return '{0}{1}'.format([(isCurrency ? 'R$ ' : ''), this.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, '$1.')]);
};

var eproControllers  = angular.module('epro.controllers', []);
var eproServices  = angular.module('epro.services', []);
var eproFactories  = angular.module('epro.factories', []);
var eproResources  = angular.module('epro.resources', []);
var eproDirectives  = angular.module('epro.directives', []);
var eproFilters = angular.module('epro.filters', []);

var epro = angular.module(
  'epro', [
    'ngResource',
    'ngAnimate',
    'ui.router',
    'ui.utils.masks',
    'ui.checkbox',
    'ngCookies',
    'ngDragDrop',
    'ui.ace',
    '19degrees.ngSweetAlert2',
    'epro.controllers',
    'epro.services',
    'epro.factories',
    'epro.resources',
    'epro.directives',
    'epro.filters',
    'ngFileUpload'
  ]
);

epro.constant('appConfig', {
  backendURL: '@@backendURL',
  appURL: '@@appURL',
  env: '@@env'
});

var TEMPLATE_FOLDER = 'templates';

epro.config(['$httpProvider', '$stateProvider', '$urlRouterProvider', function($httpProvider, $stateProvider, $urlRouterProvider) {
  moment.locale('pt-BR');

  $stateProvider
    .state({
      name: 'home',
      url: '/home',
      templateUrl: '/templates/home.html',
      controller: 'HomeController'
    });

  $urlRouterProvider.when('', '/home');
}]);

epro.run(['$rootScope', '$state', '$q', 'Notifier', 'AuthService', function($rootScope, $state, $q, Notifier, AuthService) {
 $rootScope.TEMPLATE_FOLDER = TEMPLATE_FOLDER;

  AuthService.update();

}]);
