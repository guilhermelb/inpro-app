
'use strict';

eproServices.service('AuthService',['$rootScope', '$cookies', function($rootScope, $cookies){
  this.token = null;
  this.username = null;
  this.profileKey = null;

  var self = this;


  this.update = function() {
    self.token = $cookies.get('inceresUserToken');
    self.username = $cookies.get('inceresUserName');
    self.profileKey = $cookies.get('inceresProfileKey');
  };

  this.userIsLogged = function() {
    return self.token !== null && self.username !== null;

  };

  this.clear = function() {
    self.token = null;
    self.username = null;
    self.profileKey = null;
  };
}]);
