'use strict';

eproResources.factory('Healthcheck', ['$resource', 'appConfig', function ($resource, appConfig) {
  return $resource('{0}/healthcheck'.format([appConfig.backendURL]), null, {update: {method: 'PUT'}});
}]);